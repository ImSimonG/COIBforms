from reportlab.pdfgen import canvas
from xlrd import open_workbook
from reportlab.pdfbase.pdfform import buttonFieldAbsolute
from reportlab.lib.utils import ImageReader

# to get this to work, you need to create a new directory called Excel_Two_PDFs
"""
creates pdf

figure out a way to figure data so i can draw a string from the data into the right fields

Create a filled in PDF for the COIB Data Request Form.

:param data: Field data for the COIB Request Form
:type data: List
:param filename: Name of output file.
:type filename: String
:return: Whether file was created successfully.
:rtype: Boolean
"""

# Basic Setup - Reportlab

logo = ImageReader('sylvia_signature.jpg')


def create_pdf(row_index, filers_name, year_one, year_two, third_year, sheet_name):
    c = canvas.Canvas("Excel_Three_PDFs/COIB_FORMS_" + sheet_name + "_" + str(row_index) + ".pdf")
    c.setFont("Helvetica", 9)
    # total amount of people in this PDF file are 623; the script prints all 623 from 0-623
    # Page Header
    c.drawString(178, 770, 'NEW YORK CITY CONFLICTS OF INTEREST BOARD')
    c.drawString(150, 755, 'ANNUAL DISCLOSURE REPORT INSPECTION REQUEST FORM')
    c.drawString(60, 720,
             'Pursuant to the Annual Disclosure Law, the Conflicts of Interest Board must give notice to the filer of the report')
    c.drawString(60, 705,
             'of the production and of the identify of the person to whom the report was produced. In accordance with the')
    c.drawString(60, 690,
             'notification requirement, a copy of the top portion of this form will be sent to the filer when the Board ')
    c.drawString(60, 675, 'provides the requestor with the report(s).')
    c.drawString(90, 650, 'A COPY OF THIS FORM MUST BE SUBMITTED FOR EACH FILER (NOT YEAR) REQUESTED. ')

    # Filers Name - Used to identify data being requested.
    c.drawString(60, 615, "FILER'S NAME: ")
    c.drawString(145, 617, filers_name)
    c.rect(140, 610, 380, 20)
    # Insert data for the filer's name

    # For each year, check if it is provided in the data.

    c.drawString(60, 580, "YEAR(S): ")

    # the string split method looks for "-" string and splits the two years based on the string "-"
    # then we set each year to year_one and year_two
    # we then check if either the years are 4 numbers or 2 numbers
    # for year 2002, checking if the year column has either 2002 or 0
    if year_one == "2008":
        buttonFieldAbsolute(c, "field1", "Yes", 140, 575)
    elif year_one == "08":
        buttonFieldAbsolute(c, "field1", "Yes", 140, 575)
    elif year_two == "2008":
        buttonFieldAbsolute(c, "field1", "Yes", 140, 575)
    elif year_two == "08":
        buttonFieldAbsolute(c, "field1", "Yes", 140, 575)
    else:
        buttonFieldAbsolute(c, "field1", "Off", 140, 575)
    c.rect(140, 575, 15, 15)
    c.drawString(156, 579, "2008 ")

    if year_one == "2009":
        buttonFieldAbsolute(c, "field2", "Yes", 186, 575)
    elif year_one == "09":
        buttonFieldAbsolute(c, "field2", "Yes", 186, 575)
    elif year_two == "2009":
        buttonFieldAbsolute(c, "field2", "Yes", 186, 575)
    elif year_two == "09":
        buttonFieldAbsolute(c, "field2", "Yes", 186, 575)
    else:
        buttonFieldAbsolute(c, "field2", "Off", 186, 575)
    c.rect(186, 575, 15, 15)
    c.drawString(203, 579, "2009 ")

    if year_one == "2010":
        buttonFieldAbsolute(c, "field3", "Yes", 228, 575)
    elif year_one == "10":
        buttonFieldAbsolute(c, "field3", "Yes", 228, 575)
    elif year_two == "2010":
        buttonFieldAbsolute(c, "field3", "Yes", 228, 575)
    elif year_two == "10":
        buttonFieldAbsolute(c, "field3", "Yes", 228, 575)
    else:
        buttonFieldAbsolute(c, "field3", "Off", 228, 575)
    c.rect(228, 575, 15, 15)
    c.drawString(245, 579, "2010 ")

    if year_one == "2011":
        buttonFieldAbsolute(c, "field4", "Yes", 270, 575)
    elif year_one == "11":
        buttonFieldAbsolute(c, "field4", "Yes", 270, 575)
    elif year_two == "2011":
        buttonFieldAbsolute(c, "field4", "Yes", 270, 575)
    elif year_two == "11":
        buttonFieldAbsolute(c, "field4", "Yes", 270, 575)
    else:
        buttonFieldAbsolute(c, "field4", "Off", 270, 575)
    c.rect(270, 575, 15, 15)
    c.drawString(287, 579, "2011 ")

    if year_one == "2012":
        buttonFieldAbsolute(c, "field5", "Yes", 312, 575)
    elif year_one == "12":
        buttonFieldAbsolute(c, "field5", "Yes", 312, 575)
    elif year_two == "2012":
        buttonFieldAbsolute(c, "field5", "Yes", 312, 575)
    elif year_two == "12":
        buttonFieldAbsolute(c, "field5", "Yes", 312, 575)
    elif third_year == "2012":
        buttonFieldAbsolute(c, "field5", "Yes", 312, 575)
    elif third_year == "12":
        buttonFieldAbsolute(c, "field5", "Yes", 312, 575)
    else:
        buttonFieldAbsolute(c, "field5", "Off", 312, 575)
    c.rect(312, 575, 15, 15)
    c.drawString(329, 579, "2012 ")

    if year_one == "2013":
        buttonFieldAbsolute(c, "field6", "Yes", 354, 575)
    elif year_one == "13":
        buttonFieldAbsolute(c, "field6", "Yes", 354, 575)
    elif year_two == "2013":
        buttonFieldAbsolute(c, "field6", "Yes", 354, 575)
    elif year_two == "13":
        buttonFieldAbsolute(c, "field6", "Yes", 354, 575)
    else:
        buttonFieldAbsolute(c, "field6", "Off", 354, 575)
    c.rect(354, 575, 15, 15)
    c.drawString(370, 579, "2013 ")

    if year_one == "2014":
        buttonFieldAbsolute(c, "field7", "Yes", 396, 575)
    elif year_one == "14":
        buttonFieldAbsolute(c, "field7", "Yes", 396, 575)
    elif year_two == "2014":
        buttonFieldAbsolute(c, "field7", "Yes", 396, 575)
    elif year_two == "14":
        buttonFieldAbsolute(c, "field7", "Yes", 396, 575)
    else:
        buttonFieldAbsolute(c, "field7", "Off", 396, 575)
    c.rect(396, 575, 15, 15)
    c.drawString(412, 579, "2014 ")

    if year_one == "2015":
        buttonFieldAbsolute(c, "field8", "Yes", 438, 575)
    elif year_one == "15":
        buttonFieldAbsolute(c, "field8", "Yes", 438, 575)
    elif year_two == "2015":
        buttonFieldAbsolute(c, "field8", "Yes", 438, 575)
    elif year_two == "15":
        buttonFieldAbsolute(c, "field8", "Yes", 438, 575)
    else:
        buttonFieldAbsolute(c, "field8", "Off", 438, 575)
    c.rect(438, 575, 15, 15)
    c.drawString(454, 579, "2015 ")

    if year_one == "2016":
        buttonFieldAbsolute(c, "field9", "Yes", 480, 575)
    elif year_one == "16":
        buttonFieldAbsolute(c, "field9", "Yes", 480, 575)
    elif year_two == "2016":
        buttonFieldAbsolute(c, "field9", "Yes", 480, 575)
    elif year_two == "16":
        buttonFieldAbsolute(c, "field9", "Yes", 480, 575)
    else:
        buttonFieldAbsolute(c, "field9", "Off", 480, 575)
    c.rect(480, 575, 15, 15)
    c.drawString(496, 579, "2016 ")

    # Requestor's Name - This will be standardized. It should be a constant in this file.
    c.drawString(60, 547, "REQUESTOR'S NAME: ")
    # requested by Pauline to have the Requestors name set to Sylvia Kollar
    c.drawString(165, 547, "Sylvia Kollar")
    c.rect(160, 540, 360, 20)

    # Affiliation - This will be standardized. It should be a constant in this file
    c.drawString(60, 530, "AFFILIATION (PERSON OR ENTITY ON WHOSE BEHALF YOU ARE OBTAINING THIS REPORT) IF ANY:")
    c.drawString(150, 507, "Municipal Archvist")
    c.rect(140, 500, 380, 20)

    # Entity's Address - This will be standardized. It should be a constant in this file.
    c.drawString(60, 480, "ENTITY'S ADDRESS: (DO")
    c.drawString(60, 465, "NOT   DISCLOSE   YOUR")
    c.drawString(60, 450, "HOME ADDRESS)")
    # requested by Pauline to have the Address to be 31 Chambers
    c.drawString(200, 472, "31 Chambers St, New York, NY 10007")
    c.rect(190, 465, 330, 20)
    c.rect(190, 441, 330, 20)

    # Entity's Signature - This will be standardized. It should be a constant in this file.
    c.drawString(60, 405, "REQUESTOR'S SIGNATURE:")
    c.rect(190, 395, 330, 35)
    c.drawImage(logo, 200, 396, mask='auto')
    # Entity's Date of request - This will be standardized. It
    c.drawString(60, 372, "DATE REQUESTED:")
    c.rect(190, 365, 150, 20)
    c.drawString(200, 372, "August 9th")

    # Entity's Date reports provided
    c.drawString(60, 335, "DATE REPORTS PROVIDED:")

    c.drawString(225, 335, "2008")
    c.drawString(245, 335, "_________________")
    c.drawString(370, 335, "2009")
    c.drawString(390, 335, "_________________")
    c.drawString(225, 320, "2010")
    c.drawString(245, 320, "_________________")
    c.drawString(370, 320, "2011")
    c.drawString(390, 320, "_________________")
    c.drawString(225, 305, "2012")
    c.drawString(245, 305, "_________________")
    c.drawString(370, 305, "2013")
    c.drawString(390, 305, "_________________")
    c.drawString(225, 290, "2014")
    c.drawString(245, 290, "_________________")
    c.drawString(370, 290, "2015")
    c.drawString(390, 290, "_________________")
    c.drawString(225, 275, "2016")
    c.drawString(245, 275, "_________________")

    # Everything below is for COIB use only
    c.drawString(250, 240, "FOR COIB USE ONLY")

    c.drawString(60, 210, "REQUESTOR'S TELEPHONE NO.:")
    c.drawString(225, 210, "___________________________________________________________")

    c.drawString(60, 180, "REQUESTOR'S E-MAIL ADDRESS:")
    c.drawString(225, 180, "___________________________________________________________")

    c.drawString(60, 150, "REQUESTOR'S IDENTIFICATION:")
    c.drawString(225, 150, "___________________________________________________________")

    c.drawString(60, 120, "DATE PROCESSED:")
    c.drawString(170, 120, "________________________")
    c.drawString(320, 120, "BY:")
    c.drawString(350, 120, "_________________________")

    c.drawString(60, 90, "AMOUNT RECEIVED:")
    c.drawString(190, 90, "CHECK")
    c.drawString(222, 90, "__________")
    c.drawString(290, 90, "CASH")
    c.drawString(315, 90, "__________")
    c.drawString(390, 90, "VIEWED")
    c.drawString(425, 90, "__________")

    c.save()


# MAIN FUNCTION
wb = open_workbook('2008_2012_inventory.xlsx')
values = []
for index, s in enumerate(wb.sheets()):
    # print 'Sheet:',s.name
    for row in range(1, s.nrows):
        col_names = s.row(0)
        col_value = []
        # import ipdb; ipdb.set_trace()
        for name, col in zip(col_names, range(s.ncols)):
            value = (s.cell(row, col).value)
            try:
                value = str(int(value))
            except:
                pass
            # remove name.value to get rid of headers in your tuples
            col_value.append((name.value, value))
        values.append(col_value)

for index, value in enumerate(values):
    if value[-1][-1] == "0":
        # checking if the last row & the last column is what the sheet number is
        line = value[0]
        sheet_1_existing_positions = value[1][1]
        years = value[3][1]
        years_split = years.split('-')
        first_year = years_split[0]
        second_year = years_split[1]
        third_year = years_split[2]
        print(years)
        print(sheet_1_existing_positions)
        print "First year: " + first_year
        print "Second year: " + second_year
        create_pdf(index, sheet_1_existing_positions, first_year, second_year, third_year, "Sheet0")
    if value[-1][-1] == "1":
        line = value[0]
        sheet_2_new_positions = value[1][1]
        years = value[3][1]
        years_split = years.split('-')
        first_year = years_split[0]
        second_year = None
        print(sheet_2_new_positions)
        print(years)
        print "First year: " + first_year
        create_pdf(index, sheet_2_new_positions, first_year, second_year, third_year, "Sheet1")

    if value[-1][-1] == "2":
        line = value[0]
        sheet_3_promotions = value[1][1]
        years = value[3][1]
        years_split = years.split('-')
        first_year = years_split[0]
        second_year = None
        print(sheet_3_promotions)
        print "First year: " + first_year
        create_pdf(index, sheet_3_promotions, first_year, second_year, third_year, "Sheet2")